// console.log('Hello ALz');

function pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = 1.5 * level;


	this.tackle = function(enemy){
		console.log(this.name + " tackled " + enemy.name + ".");
	}


	this.faint = function(){
		console.log(this.name + " fainted");
	};
}

let Hoenn = ["Max","May"];
let Kanto = ["Brock","Misty"];




let pkmn = {};

pkmn.name = "Ash Ketchum";
pkmn.age = 10;
pkmn.pokemon = ["Pikachu","Charizard","Squirtle","Bulbasor"];
pkmn.friend = [Hoenn,Kanto];
console.log(pkmn);

let pika = {
	name: "Pikaaa",
	talk: function(){
	console.log("Pikachu! I choose you!");
	}
}


console.log("Result of dot notation");
console.log(pkmn.name);
console.log("Result of square bracket notation");
console.log(pkmn.pokemon);
console.log("Result of talk method");
pika.talk();



let pkmn0 = new pokemon("Pickachu", 12);
console.log(pkmn0);
let pkmn1 = new pokemon("Geodude", 8);
console.log(pkmn1);
let pkmn2 = new pokemon("MewTwo", 100);
console.log(pkmn2);


pkmn1.tackle(pkmn0);
pkmn2.tackle(pkmn1);
pkmn2.faint();








